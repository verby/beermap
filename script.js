$(document).ready(function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {

      // Les variables importantes
      var latitude = position.coords.latitude;
      var longitude = position.coords.longitude;

      // AFFICHAGE DE LA CARTE
      var map = L.map('carte').setView([latitude, longitude], 5);
      var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
      var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
      var france = new L.TileLayer(osmUrl, {
        minZoom: 2,
        maxZoom: 15,
        attribution: osmAttrib
      });
      var user = L.marker([latitude, longitude]).addTo(map).bindPopup("<b>Ma position</b>");

      map.addLayer(france);

      var beers = GetBeer(map).responseJSON.records;
      var countries = GetCountries('Europe', beers).responseJSON;
      var regions=[];
      GetRegions(function(regions){
        //Create options regions
        var divRegions=document.getElementById('regions');
        for(var i=0; i<regions.length; i++){
          var a=document.createElement('button');
          a.className="dropdown-item";
          a.setAttribute('id', regions[i]);
          // a.setAttribute('onclick',"GetCountries('"+regions[i]+"');");
          a.addEventListener('click', function(e) {
            var button = e.currentTarget;
            var reg = button.getAttribute('id');
            GetCountries(reg, beers, createMenu);
          });
          a.appendChild(document.createTextNode(regions[i]));
          divRegions.appendChild(a);
        }
      });
      // Side Menu
      createMenu(countries, beers);

    });
  }
});


function createMenu(countries, beers) {
  var unique = new Set();

  beers.forEach(function(beer) {
    if (beer.fields.coordinates != null) {
      unique.add(beer.fields.country);
    }
  });
  // menu
  var elmt = document.getElementById('accordion');
  //Clean menu
  while (elmt.firstChild) {
    elmt.removeChild(elmt.firstChild);
}

  // for countries
  for (var i = 0; i < countries.length; i++) {
    unique.forEach(function(unique) {
      if (countries[i].name == unique) {
        // Creation div menu
        var card = document.createElement("div");
        card.setAttribute('class', 'card');
        elmt.appendChild(card);
        // div name countries
        var div = document.createElement("div");
        div.setAttribute('class', 'card-header');
        div.setAttribute('id', 'heading_'+ countries[i].name );
        card.appendChild(div);
        var h5 = document.createElement("h5");
        h5.setAttribute('class', 'mb-0');
        div.appendChild(h5);
        var button = document.createElement("button");
        button.setAttribute('class', 'btn btn-link');
        button.setAttribute('data-toggle', 'collapse');
        button.setAttribute('data-target', '#collapseOne'+ countries[i].name.replace(" ", '_'));
        button.setAttribute('aria-expanded', 'true');
        button.setAttribute('aria-controls', 'collapseOne');
        var t = document.createTextNode(countries[i].name); // Create a text node
        button.appendChild(t);
        h5.appendChild(button);

        //Ajout l'info de pays
        var tbl = document.createElement('table');
        tbl.style.width = '100%';
        tbl.setAttribute('border', '1');
        var tbdy = document.createElement('tbody');
        var array=['Drapeau','Capitale','Population','Langues','Monnais','Bières'];
        var capitale=countries[i].capital;
        var population=countries[i].population;
        var listLangues=countries[i].languages;
        var langues=[];
        for (var l=0;l<listLangues.length;l++){
          langues[l]=countries[i].languages[l].name;
        }
        var listMonnais=countries[i].currencies;
        var monnais=[];
        for (var l=0;l<listMonnais.length;l++){
          monnais[l]=countries[i].currencies[l].name;
        }
        var drapeau=countries[i].flag;
        var bieres=[];
        // id de la bière nommé brasserie pour ne pas confondre
        var brasserie=[];
        var coord = [];

        // ajout des bières
        var b=0;
        beers.forEach(function(beer) {
          if (countries[i].name == beer.fields.country) {
            if (beer.fields.coordinates != null) {
                  bieres[b]=beer.fields.name;
                  // id de la bière nommé brasserie pour ne pas confondre
                  brasserie[b]=beer.fields.id;
                  coord[b]=beer.fields.coordinates;
                  b++;
                }
              }
        });
        var data=[drapeau,capitale,population,langues,monnais,bieres,brasserie, coord];
        for (var d = 0; d < array.length; d++) {
          var tr = document.createElement('tr');
          var td1 = document.createElement('td');
          td1.appendChild(document.createTextNode(array[d]));
          var td2 = document.createElement('td');
         if(d===0){
              var img=document.createElement('img');
              img.src=data[d];
              img.width="130";
              img.height="80";
              td2.appendChild(img);
          }else if(data[d].constructor === Array){
            var ul=document.createElement('ul');
            for(var m=0;m<data[d].length;m++){
              // Création liste bière
              var li=document.createElement('li');
              // id de la bière
              li.setAttribute('id', data[6][m]);
              // Call GetOneBeer
              li.addEventListener('click', function(e) {
                var button = e.currentTarget;
                var reg = button.getAttribute('id');
                GetOneBeer(data);
              });
              li.appendChild(document.createTextNode(data[d][m]));
              ul.appendChild(li);
            }
            td2.appendChild(ul);
          }else{
              td2.appendChild(document.createTextNode(data[d]));
          }
          tr.appendChild(td1);
          tr.appendChild(td2);
          tbdy.appendChild(tr);
          }
        tbl.appendChild(tbdy);

        //div name beers
        var div2 = document.createElement("div");
        div2.setAttribute('id', 'collapseOne'+  countries[i].name.replace(" ", '_'));
        div2.setAttribute('class', 'collapse');
        div2.setAttribute('aria-laballendby', 'headingOne');
        div2.setAttribute('data-parent', '#accordion');
        card.appendChild(div2);
        div2.appendChild(tbl);
      }
    });
  }
}

function GetRegions(cb){
  $.ajax({
    type: 'GET',
    url: 'https://restcountries.eu/rest/v2',
    dataType: 'json',
    async: false,
    success: (function(data) {
      var regions = [];
        var i=0;
      data.forEach(function(value) {
        if(!regions.includes(value.region)){
          regions[i]=value.region;
          i++;
        }
      })
      if(cb && typeof cb === 'function') return cb(regions);
    })
  });
}

function GetCountries(region, beers, cb) {
  return $.ajax({
    type: 'GET',
    url: 'https://restcountries.eu/rest/v2/region/'+region,
    dataType: 'json',
    async: false,
    success: (function(data) {
      if(cb && typeof cb==="function"){
              cb(data, beers);
      }

    })
  });
}
// AFFICHAGE DES BIERES
function GetBeer(map, cb) {
  return $.ajax({
    "async": false,
    "crossDomain": true,
    dataType: 'json',
    "url": "https://data.opendatasoft.com/api/records/1.0/search/?dataset=open-beer-database%40public-us&facet=style_name&facet=cat_name&facet=name_breweries&facet=country&rows=500",
    "method": "GET",
    success: function(data) {
      data.records.forEach(function(value) {
        if (value.fields.coordinates != null) {

          // Variables affichables
          var lat = value.fields.coordinates[0];
          var lon = value.fields.coordinates[1];
          var name = value.fields.name;
          if (value.fields.cat_name != null) {
            var cat_name = value.fields.cat_name;
          } else {

            var cat_name = "nice beer ";
          }

          var name_breweries = value.fields.name_breweries;
          var adress = value.fields.address1;
          var city = value.fields.city;
          var country = value.fields.country;
          var marker = L.marker([lat, lon]).addTo(map);
          var content_pop = " <h4>" + name + "</h4> " +
            "<br> a " + cat_name + "make by " + name_breweries +
            "<br>localized in " + adress + "," + city + " in " + country +
            "";

          marker.bindPopup(content_pop).openPopup();
        }

      });

    },
    error: function(jqXHR, textStatus, errorThrown) {
      alert("Il y à un soucis quelque part !");
      console.log('jqXHR:');
      console.log(jqXHR);
      console.log('textStatus:');
      console.log(textStatus);
      console.log('errorThrown:');
      console.log(errorThrown);
    },
    dataType: "json",
  });
};

function GetOneBeer(beer,map){
  var lat = beer[7][0];
  var lon = beer[7][1];
  // AJOUTER LE CODE POUR CENTRER LA MAP
};
